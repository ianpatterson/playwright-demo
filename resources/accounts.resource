*** Settings ***
Library         String
Library         Browser
Resource        navigation.resource

*** Variables ***
#Top of Page
${TITLE_DASHBOARDS}                                 div[data-automation-id="titleAccounts"]
${BUTTON_EXPORT_CSV}                                a[data-automation-id="btnExportCsv"]
${BUTTON_ADD_ACCOUNT}                               a[data-automation-id="btnAddAccount"]

${ACCOUNT_TABLE}                                    div[data-automation-id="tableAccountList"]
${SEARCHBAR}                                        ${ACCOUNT_TABLE} input.njc-form-search-bar

#Account Table
${FIRST_ROW}                                        div.nc-display-table > div:nth-child(2)

#Create Account Modal
${FORM_CREATE_ACCOUNT}                              form[data-automation-id="formCreateAccount"]
${INPUT_COMPANY_NAME}                               div[data-automation-id="inputCompanyName"] input
${BUTTON_CREATE_ACCOUNT}                            button[data-automation-id="btnConfirmNewAccount"]

#Account Settings
${SETTINGS_ACCOUNT_NAME}                            div#newadvertiser h2
${SETTINGS_DELETE_BUTTON}                           div#newadvertiser div.njc-show a.njc-btn-delete
${MODAL_CONFIRM_DELETE}                             div.swal2-container > div.swal2-modal.swal2-show
${BUTTON_CONFIRM_DELETE}                            ${MODAL_CONFIRM_DELETE} button.swal2-confirm

#Command Center
${COMMAND_CENTER_AREA}                              div[data-automation-id="areaCommandCenter"]
${COMPANY_TITLE}                                    span[data-automation-id="lblCompanyTitle"]
${BUTTON_ADD_DATASOURCE}                            a[data-automation-id="btnAddDatasource"]
${BUTTON_MORE_ACTIONS}                              button#menu1
${BUTTON_SETTINGS}                                  div.dropdown.njc-dropdown.show a#btn-settings

#Data Source Modal
${TITLE_CHOOSE_DATASOURCE}                          h2[data-automation-id="titleChooseDatasource"]
${INPUT_SEARCH_DATASOURCE}                          input[data-automation-id="inputDatasourceSearch"]

#Network Settings Modal
${TITLE_NETWORK_SETTINGS}                           h2[data-automation-id="titleNetworkForm"]
${BUTTON_ADD_NETWORK}                               button[data-automation-id="btnAddNetwork"]
${BUTTON_CANCEL_NETWORK}                            button[data-automation-id="btnCancelAddNetwork"]

#Network Config Modal
${TITLE_NETWORK_CONFIG}                             h2[data-automation-id="titleNetworkForm"]
${SELECT_NETWORK_ID}                                select[name="network_id"] ~ span > span > span.select2-selection
${DROPDOWN_OPTIONS}                                 span.select2-results
#advertiserNetworkForm > div:nth-child(6) > div > div.expand.r-pad-30.njc-form-text-default-input-container > div > div > div:nth-child(2) > div
${SELECT_NETWORK_ACCOUNT_ID}                        select[name="network_account_id"] ~ span > span > span.select2-selection
${BUTTON_CONNECT_NETWORK}                           button[data-automation-id="btnConnectNetwork"]
${BUTTON_CANCEL_CONNECT}                            button[data-automation-id="btnCancelConnectNetwork"]

${SAVE_SUCCESS_POPUP}               div.toast-success

*** Keywords ***
Launch New Account Modal
    Click           ${BUTTON_ADD_ACCOUNT} 
    Validate Element is Visible    ${FORM_CREATE_ACCOUNT}

Confirm New Account Creation
    Click           ${BUTTON_CREATE_ACCOUNT}
    Validate Element is Visible    ${COMMAND_CENTER_AREA}

Insert ${name} to Company Name Field
    Fill Text       ${INPUT_COMPANY_NAME}       ${name}

Launch Add Datasource Modal
    Click           ${BUTTON_ADD_DATASOURCE}
    Validate Element is Visible    ${TITLE_CHOOSE_DATASOURCE}

Add Datasource ${name}
    ${datasource_id}=       Set Variable        div[data-automation-id="btn${name}"]
    Click           ${datasource_id}
    Validate Element is Visible    ${TITLE_NETWORK_SETTINGS}

Confirm New Network Addition
    Click           ${BUTTON_ADD_NETWORK}
    Validate Element is Visible    ${TITLE_NETWORK_CONFIG}

Configure Network
    [Documentation]         Configured for NinjaTrack "Call Tracking Network"
    Click           ${SELECT_NETWORK_ID}
    Validate Element is Visible    ${DROPDOWN_OPTIONS}
    ${first_option}=        Set Variable        ${DROPDOWN_OPTIONS} > ul > li:nth-child(2)
    Click           ${first_option}
    Take Screenshot
    Configure Network Account

Configure Network Account
    [Documentation]         Configured for NinjaTrack "Call Tracking Account"
    #Give sub-menu time to load
    Sleep           1s
    Click           ${SELECT_NETWORK_ACCOUNT_ID}
    Validate Element is Visible    ${DROPDOWN_OPTIONS}
    Take Screenshot
    ${first_option}=        Set Variable        ${DROPDOWN_OPTIONS} > ul > li:nth-child(2)
    Click           ${first_option}
    Take Screenshot

    Click           ${BUTTON_CONNECT_NETWORK}
    Validate Element is Visible    ${SAVE_SUCCESS_POPUP}
    Validate Element is Visible    ${COMMAND_CENTER_AREA}

Delete AdvertiserAccount
    Click           ${BUTTON_MORE_ACTIONS} 
    Click           ${BUTTON_SETTINGS}
    Validate Element is Visible    ${SETTINGS_ACCOUNT_NAME}
    
    Click           ${SETTINGS_DELETE_BUTTON}
    Validate Element is Visible    ${MODAL_CONFIRM_DELETE}
    Click           ${BUTTON_CONFIRM_DELETE}
    Validate Element is Visible    ${SAVE_SUCCESS_POPUP}
