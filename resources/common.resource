*** Settings ***
Library         Browser
Library         OperatingSystem
Resource        login.resource
Resource        navigation.resource

*** Variables ***
${DIRECTORY_SCREENSHOTS}       ${EXECDIR}/results/screenshots
${DIRECTORY_DOWNLOADS}         ${EXECDIR}/Downloads/
${FILE_MAILTRAP_API}           ${EXECDIR}/testData/mailtrap.json

*** Keywords ***
Prepare Browser
    New Browser     chromium    headless=True   downloadsPath=${DIRECTORY_DOWNLOADS}    args=['--always-open-pdf-externally']
    New Context     acceptDownloads=True

Validate Element is Visible
    [Arguments]    ${element}
    [Documentation]     Wait 1 minute for an element to appear, take a screenshot
    Wait For Elements State     ${element}       visible     timeout=60 s
    Take Screenshot

Take Screenshot of Full Page
    [Documentation]     Save a screenshot of the entire plage
    [Arguments]    ${image_name}
    Take Screenshot     fullPage=True     filename=${DIRECTORY_SCREENSHOTS}/${image_name}

Take Screenshot of Element
    [Documentation]     Save a screenshot of an element with a specified name. Useful for image comparison
    [Arguments]    ${element}    ${image_name}
    Take Screenshot     selector=${element}     filename=${DIRECTORY_SCREENSHOTS}/${image_name}

Wait Until Element Disappears
    [Arguments]     ${element}
    [Documentation]     Wait 1 minute until element is no longer visible
    #First check that element exists
    Validate Element is Visible  ${element}
    #Then wait for it to disappear
    Wait For Elements State    ${element}    hidden    timeout=1m

#TODO - Can extend this to a helper resource?
Log onto NinjaCat Environment with User
    [Documentation]     Specify the environment and user to log in.
    ...                 'Environment' is the folder name in testData
    ...                 'User' is derived from the users.json in that folder
    ...                 E.g., /testData/qa/url.json & 
    ...                 "automation" from /testData/qa/users.json
    [Arguments]         ${environment}      ${user}
    #Log onto Environment with User      ${environment}      ${user}
    ${json_url_file}=      Get File    ${EXECDIR}/testData/${environment}/url.json
    ${json_users_file}=    Get File    ${EXECDIR}/testData/${environment}/users.json

    ${url_object}=         Evaluate  ${json_url_file}  json
    ${user_object}=        Evaluate  ${json_users_file}  json

    Log onto NinjaCat    ${url_object["url"]}    ${user_object["${user}"]}[username]    ${user_object["${user}"]}[password]

Switch Tabs
    [Documentation]     Switch to newly opened tab
    ${tab1}=    Switch Page    NEW  

Navigate to Accounts Main Page
    Click Accounts Menu Link
    Click Accounts Sub-Menu Link
    
Navigate to Template Main Page
    Click Reporting Menu Link
    Click Template Sub-Menu Link

Navigate to Reporting Main Page
    Click Reporting Menu Link
    Click Reports Sub-Menu Link
