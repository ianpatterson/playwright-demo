expected_screenshot_list = {}

class find_in_list():
    def set_expected_screenshot_list (self, screenshot_list):
        global expected_screenshot_list
        expected_screenshot_list = screenshot_list

    def look_for (self, img_name):
        
        img_loc = [i for i, s in enumerate(expected_screenshot_list) if img_name in s][0]

        return expected_screenshot_list[img_loc]