*** Settings ***
Library         Browser
Resource        ../../resources/navigation.resource
Resource        ../../resources/login.resource
Resource        ../../resources/accounts.resource
Variables       ../testData/testData.py
Default Tags    regression  e2e

*** Variables ***
${ENVIRONMENT}          qa
${USER}                 integration
${NEW_ACCOUNT_NAME}     AutomationAdvertiser
${DATASOURCE_NAME}      NinjaTrack           
#Preload Test Data
${TESTDATA}             ${source_data}[${ENVIRONMENT}]
${USERDATA}             ${TESTDATA}[${USER}]

*** Test Cases ***
Create and Delete Account
    [Documentation]         Create and delete a new Advertiser Account
    Given I launch the app and login
    And Load the Accounts page
    And create a new account with the name ${NEW_ACCOUNT_NAME}
    Then add a new ${DATASOURCE_NAME} Datasource
    And add the new network
    Then delete the advertiser account

*** Keywords ***
I launch the app and login
    Launch Web Url ${TESTDATA["url"]}
    Login ${USERDATA["username"]} ${USERDATA["password"]}
    Validate ${NAVBAR_MAIN_MENU} is visible

Load the Accounts page
    Click Accounts Menu Link
    Click Accounts Sub-Menu Link
    ${url}=     Get Url
    Should Contain      ${url}          accounts
    Take Screenshot

Create a New Account with the name ${name}
    Launch New Account Modal
    Insert ${name} to Company Name Field
    Confirm New Account Creation

Add a new ${name} Datasource
    Launch Add Datasource Modal
    Add Datasource ${name}

Add the new network
    #Confirm New Network Addition (Not Needed?)
    Configure Network

Delete the advertiser account
    Delete AdvertiserAccount