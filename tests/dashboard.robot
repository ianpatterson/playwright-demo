*** Settings ***
Library         Browser
Resource        ../resources/navigation.resource
Resource        ../resources/common.resource
Resource        ../resources/login.resource
Resource        ../resources/dashboard.resource
Variables       ../testData/testData.py
Default Tags    regression  e2e

*** Variables ***
${ENVIRONMENT}          qa
${USER}                 automation
${DASHBOARD_NAME}       AutomationDashboard
${VIEW_NAME}            Reg - Campaign Groups
${DASHBOARD_PREVIEW}    Automation-No-Delete
${ACCOUNT_NAME}         *Regression 1
#Preload Test Data
${TESTDATA}             ${source_data}[${ENVIRONMENT}]
${USERDATA}             ${TESTDATA}[${USER}]


*** Test Cases ***
#Condense the creation process?
Create And Delete Dashboard
    [Documentation]         Validate a Dashboard can be created and deleted
    Given I launch the app and login
    And load the Dashboard page
    When I click the Create Dashboard button
    And name the dashboard ${DASHBOARD_NAME}
    And select ${VIEW_NAME} template for New Dashboard
    And save the new Dashboard
    Then delete the dashboard ${DASHBOARD_NAME} from the list

Preview Dashboard
    [Documentation]         Validate Dashbaord Preview loads
    Given I launch the app and login
    And load the Dashboard page
    And generate a Preview for ${DASHBOARD_PREVIEW}
    Then select ${ACCOUNT_NAME} to preview
    And wait for data to load

*** Keywords ***
I launch the app and login
    Launch Web Url ${TESTDATA["url"]}
    Login ${USERDATA["username"]} ${USERDATA["password"]}
    Validate Element is Visible    ${NAVBAR_MAIN_MENU}

Load the Dashboard page
    Click Reporting Menu Link
    Click Dashboard Sub-Menu Link
    ${url}=     Get Url
    Should Contain      ${url}          dashboard_profile
    Take Screenshot

I click the Create Dashboard button
    Click Create Dashboard
    Take Screenshot

Name the Dashboard ${name}
    Enter Dashboard Name ${name}
    Take Screenshot

Select ${name} Template For New Dashboard
    Search and Select ${name} View
    Take Screenshot

Save the new Dashboard
    Click Save New Template
    Take Screenshot

Delete the dashboard ${name} from the list
    Search for Dashboard ${name} on List
    Verify First Result Matches ${name}
    Delete the Dashboard ${name}
    Take Screenshot

Generate a Preview for ${name}
    Preview the Dashboard ${name}

Select ${account_name} to Preview
    Load Preview with ${account_name}

Wait for data to load
    Wait for Preview Data to Load