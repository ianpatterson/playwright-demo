*** Settings ***
Library         Browser
Library         OperatingSystem
Resource        ../resources/common.resource
Resource        ../resources/template.resource
Resource        ../resources/template_builder.resource
Resource        ../resources/utils/image_compare.resource
Default Tags    regression
Test Setup      Prepare Browser

*** Variables ***
${ENVIRONMENT}              qa
${USER}                     automation
${TEMPLATE_NAME}            Reg - Campaign Groups
${ACCOUNT_NAME}             *Regression 1

*** Test Cases ***
Verify Template Widgets Match
    [Documentation]         Validate Loaded Data Widgets in Template Matches Expected Data
    [Tags]                  skip
    Given I launch the ${ENVIRONMENT} with ${USER}
    And launch the ${TEMPLATE_NAME} template
    And load the template with data from ${ACCOUNT_NAME}
    Then take screenshots of each widget on Page 1
    And go to Page 2 of the template
    Then take screenshots of each widget on Page 2
    And go to Page 3 of the template
    Then take screenshots of each widget on Page 3
    And Compare all the screenshots

Verify Template PDF Preview Matches
    [Documentation]         Validate Loaded Data in PDF Template is consistent
    Given I launch the ${ENVIRONMENT} with ${USER}
    And launch the ${TEMPLATE_NAME} template
    And download a PDF Preview for ${ACCOUNT_NAME}
    Then compare the downloaded PDF with the expected PDF

*** Keywords ***
#Prepare Browser
#    New Browser     chromium    downloadsPath=${DIRECTORY_DOWNLOADS}    args=['--always-open-pdf-externally']
#    New Context     acceptDownloads=True

I launch the ${env} with ${user}
    Log onto NinjaCat Environment with User     ${env}      ${user}

Launch the ${name} Template
    Navigate to Template Main Page
    Search for and Launch ${name} Template

Load the Template with Data from ${name}
    Expand Account Select Dropdown
    Search for Account ${name}
    Select First Search Result

Go to Page ${num} of the Template
    Click on Page ${num} of the Reg_CG Template

Take Screenshots of Each Widget on Page ${num}
    Validate Reg_CG Page ${num} Data Has Loaded
    Capture Screenshot of Reg_CG Widgets on Page ${num} 

Compare All the Screenshots
    @{recorded_screenshots}=   List Files in Directory     ${DIRECTORY_SCREENSHOTS}    pattern=*.png    absolute=true
    Compare the Reg_CG Widgets          @{recorded_screenshots}

Download a PDF Preview for ${name}
    Load the Template with Data from ${name}
    Launch the Preview Widget Modal
    Download PDF Option for Preview

Compare the Downloaded PDF with the Expected PDF
    Compare the Template PDF Files    ${DOWNLOADED_PDF_FILE_LOCATION}