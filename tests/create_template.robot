*** Settings ***
Library         Browser
Resource        ../resources/common.resource
Resource        ../resources/template.resource
Resource        ../resources/template_builder.resource
Default Tags    regression  e2e

*** Variables ***
${ENVIRONMENT}      qa
${USER}             automation
${ACCOUNT_NAME}     Ian Test
${TEMPLATE_NAME}    Automation Template


*** Test Cases ***
#Condense the creation process?
Create And Delete Template
    [Documentation]         Validate a Template can be created and deleted
    Given I launch the ${ENVIRONMENT} with ${USER}
    And start a Template from scratch
    And add a data table widget
    And load widget with Google Ads datasource
    And load widget with Date dimensions
    And load widget with Click metrics
    And select ${ACCOUNT_NAME} Account
    And wait until data has loaded
    And save template as ${TEMPLATE_NAME}
    Then exit template builder
    And perform search for Template ${TEMPLATE_NAME}
    When I delete the template
    Then no templates should be displayed


*** Keywords ***
I launch the ${env} with ${user}
    Log onto NinjaCat Environment with User     ${env}      ${user}

Start a Template from scratch
    #common.resource
    Navigate to Template Main Page
    
    #template.resource
    Validate Templates Page Loaded
    template.Launch Template Builder
    template.Launch New Template From Scratch
    
    #template_builder.resource
    Validate Template Builder Loaded

Add a data table widget
    Create Data Table Widget
    Validate Widget is Loaded
    
Load widget with Google Ads datasource
    Expand Table Widget Data Sources options
    Select Google Ads Datasource
    Navigate Back to Main Widget Menu

Load widget with Date dimensions
    Expand Table Widget Dimensions options
    Select Date Aggregate Dimension
    Navigate Back to Main Widget Menu

Load widget with Click metrics
    Expand Table Widget Metrics options
    Select Google Ads Clicks Metrics
    Navigate Back to Main Widget Menu
    
Select ${account_name} Account
    Expand Account Select Dropdown
    Search for Account ${account_name}
    Select First Search Result

Wait until data has loaded
    Validate Widget Data Has Loaded

Save template as ${template_name}
    Select Template Settings
    Set Template Name as ${template_name}
    Save Template

Exit Template Builder
    Exit Template Builder Page
    Validate Templates Page Loaded

Perform search for Template ${template_name}
    Search for Template ${template_name}

I delete the template
    Delete First Template in List

No templates should be displayed
    Validate No Templates Displayed