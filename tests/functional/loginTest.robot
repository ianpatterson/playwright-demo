*** Settings ***
Library         Browser
Resource        ../../resources/login.resource
Resource        ../../resources/navigation.resource
Resource        ../../resources/dashboard.resource
Variables       ../testData/testData.py
Default Tags    smoke   login   regression
 
*** Variables ***
${ENVIRONMENT}                  qa
${USER}                         integration
${LEGACY_DASHBOARD_USER}        dashboard_legacy
${V1_DASHBOARD_USER}            dashboard_v1
${V2_DASHBOARD_USER}            dashboard_v2
#Preload Test Data
${TESTDATA}                     ${source_data}[${ENVIRONMENT}]


*** Test Cases ***
Successful User Login
    [Documentation]     Verify login functionality
    Given I launch "${ENVIRONMENT}" environment
    And login as "${USER}" user
    Then I see the main menu

Successful Legacy Dashboard User Login
    [Documentation]     Verify Legacy Dashboard user login
    Given I launch "${ENVIRONMENT}" environment
    And login as "${LEGACY_DASHBOARD_USER}" user
    Then I see the Legacy Dashboard

Successful V1 Dashboard User Login
    [Documentation]     Verify V1 Dashboard user login
    Given I launch "${ENVIRONMENT}" environment
    And login as "${V1_DASHBOARD_USER}" user
    Then I see the V1 Dashboard

Successful V2 Dashboard User Login
    [Documentation]     Verify V2 Dashboard user login
    Given I launch "${ENVIRONMENT}" environment
    And login as "${V2_DASHBOARD_USER}" user
    Then I see the V2 Dashboard

*** Keywords ***
I launch "${env}" environment
    Launch Web Url ${TESTDATA["url"]}

I see the main menu
    Validate ${NAVBAR_MAIN_MENU} is visible

I see the Legacy Dashboard
    Validate ${LEGACY_DASHBOARD} is visible

I see the V1 Dashboard
    Validate ${V1_DASHBOARD} is visible

I see the V2 Dashboard
    Validate ${V2_DASHBOARD} is visible

Login as "${user}" user
    ${login_info}=        Get Login ${user}
    Login ${login_info["username"]} ${login_info["password"]}
    Take Screenshot

Get Login ${user}
    [Return]           ${TESTDATA}[${user}] 