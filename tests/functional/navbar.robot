*** Settings ***
Library         Browser
Resource        ../../resources/navigation.resource
Resource        ../../resources/login.resource
Variables       ../testData/testData.py
Test Setup      Log Into App Before Starting Test
Default Tags    smoke

*** Variables ***
${ENVIRONMENT}      qa
${USER}             integration
#Preload Test Data
${TESTDATA}        ${source_data}[${ENVIRONMENT}]
${USERDATA}        ${TESTDATA}[${USER}]

*** Test Cases ***
Verify Account Section Navigation
    [Documentation]     Validate main navigation bar elements
    Given the user is logged in
    And clicks the Accounts menu link
    And clicks the Accounts sub-menu link
    Then verify the Accounts page has loaded
    And clicks the Account-Groups sub-menu link
    Then verify the Account-Groups page has loaded

#Verify User Account Navigation
#Verify Monitoring Section Navigation
#Verify Reporting Section Navigation
#Verify Settings Section Navigation
#Verify Support Navigation

*** Keywords ***
#Get called via Test Setup
Log Into App Before Starting Test
    Launch Web Url ${TESTDATA["url"]}
    Login ${USERDATA["username"]} ${USERDATA["password"]}
    
The user is logged in
    Validate ${NAVBAR_MAIN_MENU} is visible

Clicks the Accounts menu link
    Click Accounts Menu Link

Clicks the Accounts sub-menu link
    Click Accounts Sub-Menu Link

Clicks the Account-Groups sub-menu link
    Click Account Groups Sub-Menu Link

Verify the Accounts page has loaded
    ${url}=     Get Url
    Should Contain      ${url}          accounts

Verify the Account-Groups page has loaded
    ${url}=     Get Url
    Should Contain      ${url}          account-groups