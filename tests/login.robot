*** Settings ***
Library         Browser
Resource        ../resources/common.resource
Resource        ../resources/dashboard.resource
Default Tags    smoke   login   regression
Test Setup      Prepare Browser
 
*** Variables ***
${ENVIRONMENT}                  qa
${AUTOMATION_USER}              automation
${SCARLETT_DASHBOARD_USER}      dashboard_scarlett
${LEGACY_DASHBOARD_USER}        dashboard_legacy
${V1_DASHBOARD_USER}            dashboard_v1
${V2_DASHBOARD_USER}            dashboard_v2


*** Test Cases ***
Validate Scarlett User Logs in Successfully and Dashboard Loads
    [Documentation]     Verify login functionality
    Given I launch NinjaCat with an scarlett dashboard user
    Then I see the Scarlett Dashboard

Validate Legacy User Logs in Successfully and Dashboard Loads
    [Documentation]     Verify Legacy Dashboard user login
    Given I launch NinjaCat with a legacy dashboard user
    Then I see the Legacy Dashboard

Validate V1 Legacy User Logs in Successfully and Dashboard Loads
    [Documentation]     Verify V1 Dashboard user login
    [Tags]              only-me
    Given I launch NinjaCat with a v1 dashboard user
    Then I see the V1 Dashboard

Validate V2 Legacy User Logs in Successfully and Dashboard Loads
    [Documentation]     Verify V2 Dashboard user login
    Given I launch NinjaCat with a v2 dashboard user
    Then I see the V2 Dashboard

*** Keywords ***
I see the Scarlett Dashboard
    Validate Element is Visible       ${SCARLETT_DASHBOARD}
    Run Keyword And Ignore Error     Wait Until Element Disappears     ${V2_AND_SCARLETT_LOADING_IMAGE}
    Take Screenshot of Full Page      scarlett_dashboard_screenshot.png

I see the Legacy Dashboard
    Validate Element is Visible       ${LEGACY_DASHBOARD}
    Sleep       1s
    Take Screenshot of Full Page      legacy_dashboard_screenshot.png

I see the V1 Dashboard
    Validate Element is Visible       ${V1_DASHBOARD}
    Sleep       1s
    Take Screenshot of Full Page      v1_dashboard_screenshot.png

I see the V2 Dashboard
    Validate Element is Visible         ${V2_DASHBOARD}
    Run Keyword And Ignore Error     Wait Until Element Disappears     ${V2_AND_SCARLETT_LOADING_IMAGE}
    Take Screenshot of Full Page       v2_dashboard_screenshot.png

I launch NinjaCat with an scarlett dashboard user
    Log onto NinjaCat Environment with User     ${ENVIRONMENT}      ${SCARLETT_DASHBOARD_USER}

I launch NinjaCat with a legacy dashboard user
    Log onto NinjaCat Environment with User     ${ENVIRONMENT}      ${LEGACY_DASHBOARD_USER} 

I launch NinjaCat with a v1 dashboard user
    Log onto NinjaCat Environment with User     ${ENVIRONMENT}      ${V1_DASHBOARD_USER}

I launch NinjaCat with a v2 dashboard user
    Log onto NinjaCat Environment with User     ${ENVIRONMENT}      ${V2_DASHBOARD_USER}
