*** Settings ***
Library         Browser
Resource        ../resources/common.resource
Resource        ../resources/reports.resource
Resource        ../resources/template_builder.resource
Resource        ../resources/utils/mail_client.resource
Default Tags    regression  e2e

*** Variables ***
${ENVIRONMENT}          qa
${USER}                 automation
${PDF_MANUAL_REPORT}    Regression PDF Manual
${CSV_MANUAL_REPORT}    Regression CSV Manual
${PPT_MANUAL_REPORT}    Regression PPT Manual
${STATUS_READY_TO_RUN}  Ready to Run
${STATUS_READY}         Ready
${STATUS_SCHEDULED}     Scheduled
${STATUS_GETTING_DATA}  Getting Data
${EMAIL}                annotation_report@email.com


*** Test Cases ***
Verify We Can Run a PDF Report
    [Documentation]         Validate that a PDF Report runs
    Given I launch the ${ENVIRONMENT} with ${USER}
    And Load the Reports page
    And Search for ${PDF_MANUAL_REPORT}
    And Status is not ${STATUS_SCHEDULED}
    And Start running the report
    Then Verify Status becomes ${STATUS_SCHEDULED}

Verify We Can Run a CSV Report
    [Documentation]         Validate that a CSV Report runs
    Given I launch the ${ENVIRONMENT} with ${USER}
    And Load the Reports page
    And Search for ${CSV_MANUAL_REPORT}
    And Status is not ${STATUS_SCHEDULED}
    And Start running the report
    Then Verify Status becomes ${STATUS_SCHEDULED}

Verify We Can Run a PPT Report
    [Documentation]         Validate that a PowerPoint Report runs
    Given I launch the ${ENVIRONMENT} with ${USER}
    And Load the Reports page
    And Search for ${PPT_MANUAL_REPORT}
    And Status is not ${STATUS_SCHEDULED}
    And Start running the report
    Then Verify Status becomes ${STATUS_SCHEDULED}

Annotate a Report and Verify Email was Sent
    [Documentation]         Validate that a report can be annotated and the email sent
    Given I launch the ${ENVIRONMENT} with ${USER}
    And Load the Reports page
    And Search for ${PDF_MANUAL_REPORT}
    And View the Report History
    Then Select the Top Report Entry to Review
    And Add a Header Element
    When I Save and Send the New Report
    Then Confirm an Email was sent to ${EMAIL}


*** Keywords ***
I launch the ${env} with ${user}
    Log onto NinjaCat Environment with User     ${env}      ${user}

Load the Reports page
    Navigate to Reporting Main Page
    ${url}=     Get Url
    Should Contain      ${url}          schedule_reports
    Take Screenshot

Search for ${report_name}
    Perform Search for Report ${report_name}

Status is not ${status}
    Verify Status of First Report is not ${status}

Start Running the Report
    Expand More Actions
    Run First Report

View the Report History
    Expand More Actions
    Click View History

Select the Top Report Entry to Review
    Click First Review Button in History View
    Validate Template Review Loaded

Add a Header Element
    Add a New Header Widget

I Save and Send the New Report
    Click Save and Send Button

Confirm an Email was sent to ${email}
    Email Should Be Sent To ${email}
    

Verify Status becomes ${expected_status}
    [Documentation]         Wait up to 1 minute for the status to changes
    ${status}=          Set Variable            ${FIRST_REPORT_ENTRY} > div[column-index='1'] > div.njc-col-status-reports > div.njc-show-flex
    Wait Until Keyword Succeeds     60s     1000ms       Get Text     ${status}   ==      ${expected_status}
    