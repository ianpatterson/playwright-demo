# Robot Framework Automation

[TOC]

## Setup Instructions

**Mac First Steps**

 Install the following using Terminal:

 Xcode: 
 ```xcode-select --install```

 Homebrew:
 ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```

Python 3:
```brew install python```

**Note**: Because MacOS comes with Python 2 installed, Homebrew will install Python 3 has a separate entity. In order to run the `pip` commands in this document, use `python3 -m pip`

**Installation**

### [Robot Framework](https://robotframework.org/)

`pip install --upgrade robotframework`

### [Robot Framework Browser](https://robotframework-browser.org/)

`python3 -m pip install --upgrade robotframework-browser`

**Important** Intialize Robot Framework Browser with `rfbrowser init`

### [DocTest Library](https://github.com/manykarim/robotframework-doctestlibrary)

For performing screenshot and PDF comparison

Install these Homebrew formulae: `brew install imagemagick tesseract ghostscript libdmtx`

*Note*: For Linux: `apt-get install imagemagick tesseract-ocr ghostscript libdmtx0b`

`pip install --upgrade robotframework-doctestlibrary`

### [RequestsLibrary](https://github.com/MarketSquare/robotframework-requests)

For performing REST calls for Mailtrap

`pip install robotframework-djangolibrary`

## Validating Docker
There is a dockerfile here that can run tests locally. A version of this base image
is checked into the devOps/containers library for use in automation (.gitlab-ci.yml).

If you want to validate your tests work in headless mode, and in the test container in
gitlabCI, you can run the following commands:

(Note: if it's not obvious, update the path in `-v` to the proper project directory)

```shell

> docker build -t test-automation:dev .
...

> docker run --rm -it -v $HOME/code/test-automation/playwright-demo:/app test-automation:dev bash

```

## Troubleshooting
